import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_camera_video/videoPlayerScreen.dart';
import 'package:path_provider/path_provider.dart';

List<CameraDescription> cameras;

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {

  CameraController _controller;

  initCamera() async {
    _controller = CameraController(cameras[0], ResolutionPreset.high, enableAudio: true);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    initCamera();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("camera"),),
      body: cameraWidget(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if(_controller.value.isInitialized){
            if(!_controller.value.isRecordingVideo){
              _startVideoRecording();
            } else {
              _stopVideoRecording();
            }
          }
        },
        child: Icon( _controller == null || !_controller.value.isInitialized || !_controller.value.isRecordingVideo ? Icons.play_arrow : Icons.stop),
        backgroundColor: Colors.green,
      ),
    );
  }

  Widget cameraWidget(BuildContext context) {
    if (cameras == null || _controller == null || !_controller.value.isInitialized) {
      return Container();
    }
    return Center(
      child: AspectRatio(
          aspectRatio: _controller.value.aspectRatio,
          child: CameraPreview(_controller)
      ),
    );
  }

  Future _startVideoRecording() async {
    if (!_controller.value.isInitialized) {
      return null;
    }
    if (_controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await _controller.startVideoRecording();
      setState(() {});
    } on CameraException catch (e) {
      print('${e.description}');
    }
  }


  Future<void> _stopVideoRecording() async {
    if (!_controller.value.isRecordingVideo) {
      return null;
    }
    try {

      await _controller.stopVideoRecording().then((value){
        Navigator.push(context, MaterialPageRoute(builder: (context) => VideoPlayerScreen(value.path)));
        setState(() {});
      });
    } on CameraException catch (e) {
      ///_showCameraException(e);
      return null;
    }
  }

}
