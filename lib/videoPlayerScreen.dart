import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerScreen extends StatefulWidget {

  final String path;
  const VideoPlayerScreen(this.path);

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {

  VideoPlayerController _controller;
  Duration _durationVideo;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.file(
        File(widget.path))
      ..initialize().then((_) {

        _durationVideo = _controller.value.duration;
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("video player"),),
      body: Stack(
        children: [
          Center(
            child: _controller.value.initialized
                ? AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: VideoPlayer(_controller),
            )
                : Container(),
          ),
          Positioned.fill(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: FlutterSlider(
                values: [1],
                max: 100,
                min: 0,
                onDragging: (handlerIndex, lowerValue, upperValue) {
                  if(_durationVideo != null){
                    int videoInMilliseconds = _durationVideo.inMilliseconds;
                    int currentPositionInMillisecond = (videoInMilliseconds / 100 * lowerValue).toInt();
                    Duration currentPosition = Duration(milliseconds:currentPositionInMillisecond);
                    _controller.seekTo(currentPosition);
                  }
                },
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _controller.value.isPlaying
                ? _controller.pause()
                : _controller.play();
          });
        },
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
